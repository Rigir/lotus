# This script loads environment variables from .cfg.ini files in your project.
# To use this script, create a .cfg.ini file in the root directory of your project
# and define your environment variables in the [environment] section of the file. 
# The keys and values should be separated by an equal sign (=), e.g.:
#
# [environment]
# MY_VAR=foo
# ANOTHER_VAR=bar

Import("env")
import os
import configparser

# Path to your .cfg.ini files
cfg_ini_files = [f for f in os.listdir(".") if f.endswith(".cfg.ini")]

# Check if any .cfg.ini files exist
if not cfg_ini_files:
    raise Exception("No .cfg.ini files found in project directory!")

# Create ConfigParser object
config = configparser.ConfigParser()

# Read all .cfg.ini files
for cfg_ini_file in cfg_ini_files:
    config.read(cfg_ini_file)

    # Get the values from the .cfg.ini file and set them as environment variables
    for section in config.sections():
        for key, value in config.items(section):
            env.Append(CPPDEFINES=[(key.upper(), value)])