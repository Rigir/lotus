// #pragma once

// #include <Arduino.h>
// #include <memory>
// #include "App.hh"

// class AppStateManager
// {
// private:
//     std::shared_ptr<App> _app;

// public:
//     AppStateManager(std::shared_ptr<App> app) : _app(nullptr)
//     {
//         TransitionTo(app);
//     }

//     void TransitionTo(std::shared_ptr<App> app)
//     {
//         _app = app;
//         _app->setAppStateManager(std::make_shared<AppStateManager>(*this));
//     }

//     void Request1()
//     {
//         _app->setup();
//     }

//     void Request2()
//     {
//         _app->loop();
//     }
// };