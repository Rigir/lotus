// #pragma once

// #include <typeinfo>
// #include <memory>
// #include "AppStateManager.hh"

// class App
// {
// protected:
//     std::shared_ptr<AppStateManager> _appStateManager;

// public:
//     App(const std::string& name) : _name(name) {}

//     virtual void setup() = 0;
//     virtual void loop() = 0;

//     void setAppStateManager(std::shared_ptr<AppStateManager> appStateManager)
//     {
//         _appStateManager = appStateManager;
//     }

//     const std::string& getName() const {
//         return _name;
//     }

// private:
//     std::string _name;
// };
