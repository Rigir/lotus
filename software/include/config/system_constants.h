#ifndef LOTUS_INCLUDE_ETC_CONFIG_H_
#define LOTUS_INCLUDE_ETC_CONFIG_H_
// ==! You need to rename file to 'config.h' !==

// General
#ifndef LOTUS_VERSION
#define LOTUS_VERSION "0.0.0"
#endif

#ifndef LOTUS_BAUND_RATE
#define LOTUS_BAUND_RATE 115200
#endif

// Wifi credentials
#ifndef LOTUS_SSID_NAME
#define LOTUS_SSID_NAME ""
#endif

#ifndef LOTUS_SSID_PASSWORD
#define LOTUS_SSID_PASSWORD ""
#endif

#if !defined(LOTUS_BUTTON_MASK) || !defined(LOTUS_BUILT_IN_LED)
#error  Need to define pins for button and led. 
#endif

#endif // LOTUS_INCLUDE_ETC_CONFIG_H_