#pragma once

#include <freertos/semphr.h>
#include <memory>

template <typename T>
class Singleton
{
public:
    static std::shared_ptr<T> getInstance(){
        xSemaphoreTake( _mutex, portMAX_DELAY);
        if(_instance == nullptr){
            _instance = std::make_shared<T>();
        }
        return _instance;
    };
    
    void releaseInstance(){
        xSemaphoreGive( _mutex );
    };

protected:
    Singleton() = default;
    ~Singleton() = default;

private:
    Singleton(Singleton<T> &other) = delete;
    void operator=(const Singleton<T> &) = delete;

    static SemaphoreHandle_t _mutex;
    static std::shared_ptr<T> _instance;
};

template <typename T>
std::shared_ptr<T> Singleton<T>::_instance = nullptr;

template <typename T>
SemaphoreHandle_t Singleton<T>::_mutex = xSemaphoreCreateMutex();
