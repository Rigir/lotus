#include <Arduino.h>
#include <GxEPD2_BW.h>
#include <memory>
#include <vector>

#include "App.hh"
#include "AppManager.hh"

#include "../include/config/system_constants.h"
#include "../include/config/GxEPD2_display_selection.h"

#include "../include/designpattern/singleton.h"
#include "../include/assets/bitmaps/logo_LOTUS.h"
#include "../include/assets/fonts/DSEG7_Classic_Bold.h"

struct tm now;
RTC_DATA_ATTR bool SYSTEM_FIRST_BOOT = true;
RTC_DATA_ATTR bool APP_FIRST_BOOT = true;

// class SampleApp : public App
// {
// public:
//   void setup() override
//   {
//     Serial.println("config | SampleApp");
//     if (APP_FIRST_BOOT)
//     {
//       Serial.println("FULL REFRESH | SampleApp");
//       APP_FIRST_BOOT = false;
//     }
//   }

//   void loop() override
//   {
//     for (size_t i = 0; i < 5; i++)
//     {
//       Serial.println("Part REFRESH | SampleApp");
//       esp_sleep_enable_timer_wakeup(10000000);
//       esp_light_sleep_start();
//     }
//   }
// };

void printTime()
{
  getLocalTime(&now, 0);
  display.fillRect(115, 15, 85, 95, GxEPD_WHITE);
  display.setTextSize(4);
  display.setCursor(120, 49);
  display.println(&now, "%H");
  display.setCursor(120, 89);
  display.println(&now, "%M");
  display.setTextSize(1);
  display.setCursor(120, 105);
  display.print(&now, "%a. %d %b.");
  display.displayWindow(115, 15, 90, 100);
  Serial.println(&now, "%H %M %a. %d %b.");
}

int setUnixtime(int32_t unixtime)
{
  timeval epoch = {unixtime, 0};
  return settimeofday((const timeval *)&epoch, nullptr);
}

void setup()
{
  Serial.begin(LOTUS_BAUND_RATE);
  // Serial.flush();
  // display.init(LOTUS_BAUND_RATE);
  // display.flush();

  // display.setRotation(3);
  // display.setTextColor(GxEPD_BLACK);
  // display.setFont(&DSEG7_Classic_Bold_7pt7b);

  // display.setFullWindow();
  // display.fillScreen(GxEPD_WHITE);

  // display.drawBitmap(2, 3, logoLOTUS_100x116, 100, 116, GxEPD_BLACK);
  // display.display();

  // std::shared_ptr<AppManager> appSwitcher = AppManager::getInstance();
  // appSwitcher->registerApp(std::make_shared<SampleApp>());
  // appSwitcher->run(CURRENT_APP_INDEX);
  // appSwitcher->releaseInstance();

  esp_sleep_wakeup_cause_t wakeup_reason = ESP_SLEEP_WAKEUP_UNDEFINED;
  wakeup_reason = esp_sleep_get_wakeup_cause();

  switch (wakeup_reason)
  {
  case ESP_SLEEP_WAKEUP_EXT0:
    Serial.println("Wakeup caused by external signal using RTC_IO");
    break;
  case ESP_SLEEP_WAKEUP_EXT1:
    Serial.println("Wakeup caused by external signal using RTC_CNTL");
    break;
  default:
    if (SYSTEM_FIRST_BOOT)
    {
      Serial.println("SYSTEM_FIRST_BOOT");
      setUnixtime(LOTUS_CURRENT_UNIX_TIMESTAMP);
      SYSTEM_FIRST_BOOT = false;
    }
    Serial.printf("\nWakeup was not caused by deep sleep: %d\n", wakeup_reason);
    break;
  }
  esp_sleep_enable_ext1_wakeup(LOTUS_BUTTON_MASK, ESP_EXT1_WAKEUP_ALL_LOW);
  esp_deep_sleep_start();
}

void loop() {}